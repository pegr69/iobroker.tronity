"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
const tronity = require("tronity-platform");
const utils = require("@iobroker/adapter-core");
class Tronity extends utils.Adapter {
    constructor(options = {}) {
        super(Object.assign(Object.assign({}, options), { name: 'tronity' }));
        this.on('ready', this.onReady.bind(this));
        this.on('message', this.onMessage.bind(this));
        this.on('stateChange', this.onStateChange.bind(this));
        this.on('unload', this.onUnload.bind(this));
        // Timeouts and intervals
        this.RefreshTokenTimeout = null;
        this.GetAllInfoTimeout = null;
    }
    /**
     * Is called when databases are connected and adapter received configuration.
     */
    onReady() {
        return __awaiter(this, void 0, void 0, function* () {
            this.log.debug('Starting Tronity');
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const Adapter = this;
            this.subscribeStates('command.*');
            yield Adapter.setStateAsync('info.connection', false, true);
            if (this.config.client_id && this.config.client_secret && this.config.vehicle_id) {
                yield Adapter.setStateAsync('info.connection', true, true);
                yield Adapter.initSetObject('command.Charging', 'boolean', 'data');
                yield Adapter.initSetObject('level', 'number', 'data');
                yield Adapter.initSetObject('usableLevel', 'number', 'data');
                yield Adapter.initSetObject('range', 'number', 'data');
                yield Adapter.initSetObject('speed', 'number', 'data');
                yield Adapter.initSetObject('power', 'number', 'data');
                yield Adapter.initSetObject('chargerPower', 'number', 'data');
                yield Adapter.initSetObject('phases', 'number', 'data');
                yield Adapter.initSetObject('voltage', 'number', 'data');
                yield Adapter.initSetObject('current', 'number', 'data');
                yield Adapter.initSetObject('energyAdded', 'number', 'data');
                yield Adapter.initSetObject('milesAdded', 'number', 'data');
                yield Adapter.initSetObject('socMax', 'number', 'data');
                yield Adapter.initSetObject('consumption', 'number', 'data');
                yield Adapter.initSetObject('outTemp', 'number', 'data');
                yield Adapter.initSetObject('inTemp', 'number', 'data');
                yield Adapter.initSetObject('passTemp', 'number', 'data');
                yield Adapter.initSetObject('limitSoc', 'number', 'data');
                yield Adapter.initSetObject('elevation', 'number', 'data');
                yield Adapter.initSetObject('fan', 'number', 'data');
                yield Adapter.initSetObject('climate', 'boolean', 'data');
                yield Adapter.initSetObject('seatHeating', 'number', 'data');
                yield Adapter.initSetObject('preconditioning', 'boolean', 'data');
                yield Adapter.initSetObject('charging', 'string', 'data');
                yield Adapter.initSetObject('driving', 'boolean', 'data');
                yield Adapter.initSetObject('wakeup', 'boolean', 'data');
                yield Adapter.initSetObject('supercharger', 'string', 'data');
                yield Adapter.initSetObject('doorLocked', 'boolean', 'data');
                yield Adapter.initSetObject('sentryMode', 'boolean', 'data');
                yield Adapter.initSetObject('windowsLocked', 'boolean', 'data');
                yield Adapter.initSetObject('odometer', 'number', 'data');
                yield Adapter.initSetObject('vehicleStatusTime', 'string', 'data');
                yield Adapter.initSetObject('vehicleLocationTime', 'string', 'data');
                yield Adapter.initSetObject('tripCurrentId', 'string', 'data');
                yield Adapter.initSetObject('tripTravelTime', 'string', 'data');
                yield Adapter.initSetObject('latitude', 'string', 'data');
                yield Adapter.initSetObject('longitude', 'string', 'data');
                yield Adapter.RefreshTokenTask();
                yield Adapter.GetAllInfoTask();
            }
        });
    }
    initSetObject(name, type, role) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.setObjectNotExistsAsync(name, {
                type: 'state',
                common: {
                    name,
                    type,
                    role,
                    write: true,
                    read: true
                },
                native: {}
            });
        });
    }
    RefreshTokenTask() {
        return __awaiter(this, void 0, void 0, function* () {
            const Adapter = this;
            try {
                const token = yield new tronity.AuthenticationApi().authControllerAuthentication({
                    client_id: Adapter.config.client_id,
                    client_secret: Adapter.config.client_secret,
                    grant_type: 'app'
                });
                Adapter.config.authToken = token.data.access_token;
                this.RefreshTokenTimeout = setTimeout(() => Adapter.RefreshTokenTask(), 59 * 60 * 1000);
            }
            catch (e) {
                Adapter.log.error(e);
            }
        });
    }
    GetAllInfoTask() {
        return __awaiter(this, void 0, void 0, function* () {
            const Adapter = this;
            try {
                const record = yield new tronity.VehiclesApi({
                    accessToken: Adapter.config.authToken
                }).getManyBaseRecordControllerRecord(Adapter.config.vehicle_id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);
                const recordData = record.data.data;
                if (recordData.length > 0) {
                    Adapter.setState('level', recordData[0].level, true);
                    Adapter.setState('usableLevel', recordData[0].usableLevel, true);
                    Adapter.setState('range', recordData[0].range, true);
                    Adapter.setState('speed', recordData[0].speed, true);
                    Adapter.setState('power', recordData[0].power, true);
                    Adapter.setState('chargerPower', recordData[0].chargerPower, true);
                    Adapter.setState('phases', recordData[0].phases, true);
                    Adapter.setState('voltage', recordData[0].voltage, true);
                    Adapter.setState('current', recordData[0].current, true);
                    Adapter.setState('energyAdded', recordData[0].energyAdded, true);
                    Adapter.setState('milesAdded', recordData[0].milesAdded, true);
                    Adapter.setState('socMax', recordData[0].socMax, true);
                    Adapter.setState('consumption', recordData[0].consumption, true);
                    Adapter.setState('outTemp', recordData[0].outTemp, true);
                    Adapter.setState('inTemp', recordData[0].inTemp, true);
                    Adapter.setState('passTemp', recordData[0].passTemp, true);
                    Adapter.setState('limitSoc', recordData[0].limitSoc, true);
                    Adapter.setState('elevation', recordData[0].elevation, true);
                    Adapter.setState('fan', recordData[0].fan, true);
                    Adapter.setState('climate', recordData[0].climate, true);
                    Adapter.setState('seatHeating', recordData[0].seatHeating, true);
                    Adapter.setState('preconditioning', recordData[0].preconditioning, true);
                    Adapter.setState('charging', recordData[0].charging, true);
                    Adapter.setState('driving', recordData[0].driving, true);
                    Adapter.setState('wakeup', recordData[0].wakeup, true);
                    Adapter.setState('supercharger', recordData[0].supercharger, true);
                    Adapter.setState('doorLocked', recordData[0].doorLocked, true);
                    Adapter.setState('sentryMode', recordData[0].sentryMode, true);
                    Adapter.setState('windowsLocked', recordData[0].windowsLocked, true);
                    Adapter.setState('odometer', recordData[0].odometer, true);
                    Adapter.setState('vehicleStatusTime', recordData[0].vehicleStatusTime, true);
                    Adapter.setState('vehicleLocationTime', recordData[0].vehicleLocationTime, true);
                    Adapter.setState('tripCurrentId', recordData[0].tripCurrentId, true);
                    Adapter.setState('tripTravelTime', recordData[0].tripTravelTime, true);
                    Adapter.setState('latitude', recordData[0].latitude, true);
                    Adapter.setState('longitude', recordData[0].longitude, true);
                }
            }
            catch (e) {
                Adapter.log.error(e);
            }
            this.GetAllInfoTimeout = setTimeout(() => Adapter.GetAllInfoTask(), 60 * 1000);
        });
    }
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    onMessage(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const Adapter = this;
            if (msg.command === 'validate') {
                const client_id = msg.message.client_id;
                const client_secret = msg.message.client_secret;
                Adapter.log.info('Try to validate login data and get vehicles');
                try {
                    const token = yield new tronity.AuthenticationApi().authControllerAuthentication({
                        client_id,
                        client_secret,
                        grant_type: 'app'
                    });
                    const vehicles = yield new tronity.VehiclesApi({
                        accessToken: token.data.access_token
                    }).getManyBaseVehicleControllerVehicle();
                    Adapter.sendTo(msg.from, msg.command, { success: true, vehicles: vehicles.data.data }, msg.callback);
                }
                catch (e) {
                    Adapter.sendTo(msg.from, msg.command, { success: false }, msg.callback);
                }
            }
        });
    }
    onStateChange(id, state) {
        return __awaiter(this, void 0, void 0, function* () {
            const Adapter = this;
            if (!state)
                return;
            Adapter.log.debug(`State Change: ${id} to ${state.val} ack ${state.ack}`);
            const State = yield Adapter.getStateAsync('info.connection');
            if (!State || !State.val) {
                Adapter.log.warn('You tried to set a State, but there is currently no valid Token, please configure Adapter first!');
                return;
            }
            const currentId = id.substring(Adapter.namespace.length + 1);
            switch (currentId) {
                case 'command.Charging':
                    if (state.val) {
                        try {
                            yield new tronity.VehiclesApi({
                                accessToken: Adapter.config.authToken
                            }).vehicleControllerChargeStart(Adapter.config.vehicle_id);
                            Adapter.log.info('Try to start charging!');
                        }
                        catch (e) {
                            Adapter.log.error(e);
                        }
                    }
                    else {
                        try {
                            yield new tronity.VehiclesApi({
                                accessToken: Adapter.config.authToken
                            }).vehicleControllerChargeStop(Adapter.config.vehicle_id);
                            Adapter.log.info('Try to stop charging!');
                        }
                        catch (e) {
                            Adapter.log.error(e);
                        }
                    }
                    break;
            }
        });
    }
    /**
     * Is called when adapter shuts down - callback has to be called under any circumstances!
     */
    onUnload(callback) {
        const Adapter = this;
        try {
            this.log.info('cleaned everything up...');
            if (Adapter.RefreshTokenTimeout) {
                clearTimeout(Adapter.RefreshTokenTimeout);
            }
            if (Adapter.GetAllInfoTimeout) {
                clearTimeout(Adapter.GetAllInfoTimeout);
            }
            callback();
        }
        catch (e) {
            callback();
        }
    }
}
if (module.parent) {
    // Export the constructor in compact mode
    module.exports = (options) => new Tronity(options);
}
else {
    // otherwise start the instance directly
    (() => new Tronity())();
}
